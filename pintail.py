import gi

from gi.repository import GObject
from gi.repository import Gio
from gi.repository import Gtk
from gi.repository import GtkSource
from gi.repository import Ide

class PintailBuildSystem(Ide.Object, Ide.BuildSystem, Gio.AsyncInitable):
    def do_init_async(self, priority, cancel, callback, data=None):
        task = Gio.Task.new(self, cancel, callback)
        task.set_priority(priority)
        task.return_boolean(True)

    def do_init_finish(self, result):
        return result.propagate_boolean()

    def do_get_build_targets_async(self, cancellable, callback, data=None):
        task = Gio.Task.new(self, cancellable, callback)
        task.build_targets = ['pintail']
        task.return_boolean(True)

    def do_get_build_targets_finish(self, result):
        if result.propagate_boolean():
            return result.build_targets

class PintailPipelineAddin(Ide.Object, Ide.BuildPipelineAddin):
    def do_load(self, pipeline):
        context = pipeline.get_context()
        build_system = context.get_build_system()
        if type(build_system) != PintailBuildSystem:
            return

        srcdir = pipeline.get_srcdir()

        pintail_launcher = pipeline.create_launcher()
        pintail_launcher.push_argv('pintail')
        pintail_launcher.push_argv('build')
        pintail_launcher.push_argv('-v')
        pintail_launcher.set_cwd(srcdir)
        pintail_stage = Ide.BuildStageLauncher.new(context, pintail_launcher)
        self.track(pipeline.connect(Ide.BuildPhase.BUILD, 0, pintail_stage))
